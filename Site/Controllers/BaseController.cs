﻿using Site.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Models;

namespace Site.Controllers
{
    public class BaseController : Umbraco.Web.Mvc.RenderMvcController, IBaseController
    {
        public IBaseItem Model { get; set; }

        public virtual string ViewPath => "Unassigned";

        public virtual void FillModel() => throw new NotImplementedException("BaseController derived class must implement FillModel()");

        public override ActionResult Index(RenderModel model)
        {
            FillModel();
            
            return View($"~/Views/{ViewPath}.cshtml", Model);
        }
    }
}