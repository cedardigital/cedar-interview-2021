﻿using Site.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web;
using Umbraco.Web.Models;

namespace Site.Controllers
{
    public class HomeController : BaseController
    {
        public override string ViewPath => "Home";

        public override void FillModel()
        {
            var homeModel = new Home();
            homeModel.DatePublished = CurrentPage.UpdateDate;
            homeModel.Title = CurrentPage.GetPropertyValue<string>("Title");
            Model = homeModel;
        }
    }
}