Feel free to contact me at andrew.walker@cedarcom.co.uk with any issues/clarifications

Note: Should take up to an hour, hopefully there are no setup issues. Please let me know if you run into anything that's blocking

Requirements
Visual Studio (2019 Community edition is free)
.NET Framework 4.7.2 (picking 4.7.1 and 4.8 during the install of Visual studio worked fine with the test project)
Means to clone a Bitbucket source repo, branch, commit and push (git bash etc)

Steps

Perform a clone of the repo: git clone https://bitbucket.org/cedardigital/cedar-interview-2021.git
Create a branch off master for your work

[Web.config is already configured - Database should be accessible over the internet]

Umbraco username/password: monkler@monkler.com l3tm3inpls

Add an Article type to the build with Title and Content fields [Content is  WYSIWYG]
Create a CSHTML view for it
Ensure a test article item renders correctly (title/content at least)
Update the solution to output a JSON representation of the model. There is a placeholder for this in _Layout.cshtml (see "All Page data in JSON" comment). Note: Please ensure all the properties within the JSON dump are output in camelCase (including any inherited properties)
Commit and push your branch

Note:
A roslyn compiler error (Roslyn csc.exe) has occurred on occasion when setting this project up on certain platforms. Reinstalling the package from Nuget seems to fix this.
Update-Package -reinstall -Id Microsoft.CodeDom.Providers.DotNetCompilerPlatform


